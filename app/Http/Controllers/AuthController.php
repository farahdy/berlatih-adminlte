<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }
    // public function welcome() {
    //     return view('welcome');
    // }
    public function welcome(Request $request) {
        $fname = $request -> first;
        $lname = $request -> last;
        return view('welcome', compact('fname', 'lname'));
    }
}
