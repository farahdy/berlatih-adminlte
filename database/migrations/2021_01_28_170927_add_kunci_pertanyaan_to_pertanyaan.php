<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKunciPertanyaanToPertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pertanyaan', function (Blueprint $table) {
            $table->unsignedBigInteger('profil_id');
            $table->unsignedBigInteger('jawaban_tepat_id');

            $table->foreign('profil_id')->references('id')->on('profile');
            $table->foreign('jawaban_tepat_id')->references('id')->on('jawaban');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pertanyaan', function (Blueprint $table) {
            $table->dropColoumn(['profil_id']);
            $table->dropColoumn(['jawaban_tepat_id']);
            $table->dropForeign(['profil_id']);
            $table->dropForeign(['jawaban_tepat_id']);
        });
    }
}
