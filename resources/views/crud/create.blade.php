@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Pertanyaan</h3>
        </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/pertanyaan" method="POST">
              @csrf
              <div class="card-body mt-3">
                <div class="form-group ml-3 mr-3">
                  <label for="title">Judul Pertanyaan</label>
                  <input type="text" class="form-control" id="title" name="judul" value="{{ old('judul', '')}}" placeholder="Enter judul" required>
                  @error('title')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
                <div class="form-group ml-3 mr-3">
                  <label for="body">Isi pertanyaan</label>
                  <input type="text" class="form-control" id="body" name="isi" value="{{ old('isi', '')}}" placeholder="Enter isi" required>
                  @error('body')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer ml-3 mb-3">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
    </div>
</div>
@endsection