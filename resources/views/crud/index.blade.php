@extends('adminlte.master')

@section('content')
    <div class= "mt-3 ml-3">
    <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Tabel Pertanyaan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                @if(session('success'))
                    <div class ="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            <a class="btn btn-info mb-3" href="/pertanyaan/create">Buat Pertanyaan</a>
              <table class="table table-bordered">
                <tbody><tr>
                  <th style="width: 10px">No</th>
                  <th>Judul</th>
                  <th>Isi</th>
                  <th style="width: 40px">Action</th>
                </tr>
                @forelse($pertanyaan as $key => $value)
                    <tr>
                        <td> {{$key + 1 }} </td>
                        <td> {{ $value -> judul}} </td>
                        <td> {{ $value -> isi}} </td>
                        <td style="display: flex;">
                            <a href="/pertanyaan/{{$value->id}}" class="btn btn-info btn-sm">show</a>
                            <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-default btn-sm ml-2">edit</a>
                            <form action="/pertanyaan/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm ml-2">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan = "4" align="center">Tidak Ada Pertanyaan</td>
                    </tr>
                @endforelse
              </tbody></table>
            </div>
        </div>
    </div>
@endsection